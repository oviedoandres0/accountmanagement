package com.neoris.services.accountmanagement.api.validators;

import com.neoris.services.accountmanagement.api.annotations.EnumValidation;
import com.neoris.services.accountmanagement.api.exceptions.ValidateServiceException;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ValidationException;

@Slf4j
public class EnumValidator implements ConstraintValidator<EnumValidation, String> {

    private EnumValidation annotation;

    @Override
    public void initialize(EnumValidation constraintAnnotation) {
        this.annotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean result = false;
        if (value == null) {
            value = "";
        }
        if (this.annotation.required()) {
            Object[] enumValues = this.annotation.enumClass().getEnumConstants();
            if (enumValues != null) {
                for (Object enumValue : enumValues) {
                    if (value.equals(enumValue.toString())
                            || (this.annotation.ignoreCase() && value.equalsIgnoreCase(enumValue.toString()))) {
                        result = true;
                        break;
                    }
                }
            }
        } else {
            return true;
        }
        return result;
    }
}
