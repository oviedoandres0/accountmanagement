package com.neoris.services.accountmanagement.api.converters;
import com.neoris.services.accountmanagement.api.dtos.UpdateAccountDTO;
import com.neoris.services.accountmanagement.api.entity.Account;

public class UpdateAccountConverter extends AbstractConverter<Account, UpdateAccountDTO> {

    @Override
    public UpdateAccountDTO convertEntityToDTO(Account entity) {
        if (entity == null) return null;

        return UpdateAccountDTO.builder()
                .id(entity.getId())
                .initialBalance(entity.getInitialBalance())
                .type(entity.getType())
                .build();
    }

    @Override
    public Account convertDTOToEntity(UpdateAccountDTO dto) {
        if (dto == null) return null;

        return Account.builder()
                .id(dto.getId())
                .initialBalance(dto.getInitialBalance())
                .type(dto.getType())
                .build();
    }
}
