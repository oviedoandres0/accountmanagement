package com.neoris.services.accountmanagement.api.enums;

public enum MovementType {
    CREDIT, DEBIT
}
