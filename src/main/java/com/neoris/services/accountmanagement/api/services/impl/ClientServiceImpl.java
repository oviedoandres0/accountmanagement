package com.neoris.services.accountmanagement.api.services.impl;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.neoris.services.accountmanagement.api.dtos.AccountReportDTO;
import com.neoris.services.accountmanagement.api.entity.Client;
import com.neoris.services.accountmanagement.api.exceptions.BusinessServiceException;
import com.neoris.services.accountmanagement.api.exceptions.NoDataFoundException;
import com.neoris.services.accountmanagement.api.repositories.ClientRepository;
import com.neoris.services.accountmanagement.api.services.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Client save(Client client) {
        try {
            // Encode the password
            client.setPassword(passwordEncoder.encode(client.getPassword()));
            // Set the status
            client.setState(true);
            // Validate if new identification not exist
            Optional<Client> clientValidation = clientRepository.findByIdentification(client.getIdentification());
            if (clientValidation.isPresent())
                throw new BusinessServiceException("The Identification is unique, please check the new identification number");
            client.setIdentification(client.getIdentification());
            // Save the client
            return clientRepository.save(client);
        } catch (BusinessServiceException e) {
            log.error("[ERROR:ec009]: ClientService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec010]: ClientService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }

    @Override
    public Client update(Client clientToUpdate) {
        try {
            // Find the Client
            Client client = clientRepository.findById(clientToUpdate.getId())
                    .orElseThrow(() -> new NoDataFoundException("Client not found"));

            // Update the object
            client.setName(clientToUpdate.getName());
            client.setGender(clientToUpdate.getGender());
            client.setAge(clientToUpdate.getAge());
            client.setAddress(clientToUpdate.getAddress());
            client.setPhone(clientToUpdate.getPhone());
            client.setPassword(passwordEncoder.encode(clientToUpdate.getPassword()));

            // Validate if new identification not exist
            if (client.getIdentification() != clientToUpdate.getIdentification()) {
                Optional<Client> clientValidation = clientRepository.findByIdentification(clientToUpdate.getIdentification());
                if (clientValidation.isPresent())
                    throw new BusinessServiceException("The Identification is unique, please check the new identification number");
                client.setIdentification(clientToUpdate.getIdentification());
            }

            // Save the client
            return clientRepository.save(client);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec011]: ClientService:update: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (BusinessServiceException e) {
            log.error("[ERROR:ec012]: ClientService:update: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec013]: ClientService:update: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }

    @Override
    public List<AccountReportDTO> getReport(String clientId, LocalDateTime startDate, LocalDateTime endDate) {
        try {
            // Find the Client
            Client client = clientRepository.findById(clientId)
                    .orElseThrow(() -> new NoDataFoundException("Client not found"));

            // Report List
            List<AccountReportDTO> report = new ArrayList<>();

            // Get the Report Response
            List<Object[]> reportResponse = clientRepository.getReport(clientId, startDate, endDate);

            // Build the Report
            if (!reportResponse.isEmpty()) {
                for (Object[] tuple : reportResponse) {
                    AccountReportDTO ar = AccountReportDTO.builder()
                            .movementDate((Timestamp) tuple[0])
                            .clientName((String) tuple[1])
                            .accountNumber((BigInteger) tuple[2])
                            .accountType((String) tuple[3])
                            .accountInitialBalance((BigInteger) tuple[4])
                            .accountState((boolean) tuple[5])
                            .movementValue((BigInteger) tuple[6])
                            .movementBalance((BigInteger) tuple[7])
                            .build();
                    report.add(ar);
                }
            }
            return report;
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec014]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec015]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }

    @Override
    public void delete(String clientId) {
        try {
            // Find the Client
            Client client = clientRepository.findById(clientId)
                    .orElseThrow(() -> new NoDataFoundException("Client not found"));
            // Delete client
            clientRepository.delete(client);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec016]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (DataIntegrityViolationException e) {
            log.error("[ERROR:ec017]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw new BusinessServiceException("The client has associated records, it is not possible to delete");
        } catch (Exception e) {
            log.error("[ERROR:ec018]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }
}
