package com.neoris.services.accountmanagement.api.services;

import com.neoris.services.accountmanagement.api.entity.Movement;

public interface MovementService {

    Movement save(Movement movement);
    void delete(String movementId);
}
