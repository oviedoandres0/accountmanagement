package com.neoris.services.accountmanagement.api.repositories;

import com.neoris.services.accountmanagement.api.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, String> {

    Optional<Client> findByIdentification(Long identification);

    @Query(value = "SELECT mv.date, pr.name, ac.number, ac.type, ac.initial_balance, ac.state, mv.value, mv.balance FROM person pr INNER JOIN client cl ON pr.id = cl.person_id INNER JOIN account ac ON cl.person_id = ac.client_id INNER JOIN movement mv ON mv.account_id = ac.id WHERE pr.id = ?1 AND mv.date BETWEEN ?2 and ?3 ORDER BY mv.date DESC", nativeQuery = true)
    List<Object[]> getReport(String clientId, LocalDateTime startDate, LocalDateTime endDate);
}
