package com.neoris.services.accountmanagement.api.controllers;

import com.neoris.services.accountmanagement.api.converters.MovementConverter;
import com.neoris.services.accountmanagement.api.dtos.MovementDTO;
import com.neoris.services.accountmanagement.api.entity.Movement;
import com.neoris.services.accountmanagement.api.services.MovementService;
import com.neoris.services.accountmanagement.api.utils.WrapperResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/movements")
@Validated
@Slf4j
public class MovementController {

    @Autowired
    private MovementService movementService;

    @Autowired
    private MovementConverter movementConverter;

    @PostMapping
    public ResponseEntity<WrapperResponse<MovementDTO>> save (@RequestBody @Valid MovementDTO movementDTORequest) {
        Movement newMovement = movementService.save(movementConverter.convertDTOToEntity(movementDTORequest));
        MovementDTO newMovementDTO = movementConverter.convertEntityToDTO(newMovement);
        log.info("Movement Created Successfully with ID: " + newMovementDTO.getId());

        return new WrapperResponse<>(true, "Movement created successfully", newMovementDTO)
                .createResponse(HttpStatus.CREATED);
    }

    @DeleteMapping
    @RequestMapping("/{id}")
    public ResponseEntity<WrapperResponse<String>> delete (@PathVariable("id") String movementId) {
        movementService.delete(movementId);
        log.info("Movement Deleted Successfully with ID: " + movementId);

        return new WrapperResponse<>(true, "Movement deleted successfully", "")
                .createResponse(HttpStatus.CREATED);
    }
}
