package com.neoris.services.accountmanagement.api.services;

import com.neoris.services.accountmanagement.api.entity.Account;


public interface AccountService {
    Account save(Account account);
    Account update(Account accountToUpdate);
    void delete(String accountId);
}
