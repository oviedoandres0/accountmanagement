package com.neoris.services.accountmanagement.api.services;

import com.neoris.services.accountmanagement.api.dtos.AccountReportDTO;
import com.neoris.services.accountmanagement.api.entity.Client;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface ClientService {

    Client save(Client client);
    Client update(Client clientToUpdate);
    List<AccountReportDTO> getReport(String clientId, LocalDateTime startDate, LocalDateTime endDate);
    void delete(String clientId);
}
