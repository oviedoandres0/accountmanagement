package com.neoris.services.accountmanagement.api.converters;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractConverter<E, D> {

    public abstract D convertEntityToDTO(E entity);
    public abstract E convertDTOToEntity(D dto);

    public List<D> convertEntityListToDTOList(List<E> entities) {
        return entities.stream()
                .map(e -> convertEntityToDTO(e))
                .collect(Collectors.toList());
    }

    public List<E> convertDTOListToEntityList(List<D> dtos) {
        return dtos.stream()
                .map(e -> convertDTOToEntity(e))
                .collect(Collectors.toList());
    }
}
