package com.neoris.services.accountmanagement.api.services.impl;

import com.neoris.services.accountmanagement.api.dtos.AccountReportDTO;
import com.neoris.services.accountmanagement.api.entity.Account;
import com.neoris.services.accountmanagement.api.entity.Client;
import com.neoris.services.accountmanagement.api.exceptions.BusinessServiceException;
import com.neoris.services.accountmanagement.api.exceptions.NoDataFoundException;
import com.neoris.services.accountmanagement.api.repositories.AccountRepository;
import com.neoris.services.accountmanagement.api.repositories.ClientRepository;
import com.neoris.services.accountmanagement.api.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account save(Account account) {
        try {
            // Find the account owner
            Client client = clientRepository.findById(account.getClient().getId())
                    .orElseThrow(() -> new NoDataFoundException("The client with ID: " + account.getClient().getId() +
                            " Not exists"));

            // Validate if Account not exits with AccountNumber
            Optional<Account> accountValidation = accountRepository.findByNumber(account.getNumber());
            if (accountValidation.isPresent())
                throw new BusinessServiceException("The Account number is unique, please check the account number");

            // Set the status and client (Account Owner)
            account.setState(true);
            account.setClient(client);

            // Save the Account
            return accountRepository.save(account);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec001]: AccountService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (BusinessServiceException e) {
            log.error("[ERROR:ec002]: AccountService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec003]: AccountService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }

    @Override
    public Account update(Account accountToUpdate) {
        try {
            // Find the account
            Account account = accountRepository.findById(accountToUpdate.getId())
                    .orElseThrow(() -> new NoDataFoundException("The Account with ID: " + accountToUpdate.getId() +
                            " Not exists"));
            // Set the type and initialBalance
            account.setType(accountToUpdate.getType());
            account.setInitialBalance(accountToUpdate.getInitialBalance());

            // Save the Account
            return accountRepository.save(account);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec004]: AccountService:update: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec005]: AccountService:update: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }

    @Override
    public void delete(String accountId) {
        try {
            // Find the Account
            Account account = accountRepository.findById(accountId)
                    .orElseThrow(() -> new NoDataFoundException("The Account with ID: " + accountId +
                            " Not exists"));
            // Delete Account
            accountRepository.delete(account);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec006]: AccountService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (DataIntegrityViolationException e) {
            log.error("[ERROR:ec007]: AccountService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw new BusinessServiceException("The account has associated records, it is not possible to delete");
        } catch (Exception e) {
            log.error("[ERROR:ec008]: AccountService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }
}
