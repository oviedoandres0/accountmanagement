package com.neoris.services.accountmanagement.api.converters;

import com.neoris.services.accountmanagement.api.dtos.MovementDTO;
import com.neoris.services.accountmanagement.api.entity.Movement;
import org.springframework.beans.factory.annotation.Autowired;

public class MovementConverter extends AbstractConverter<Movement, MovementDTO> {

    @Autowired
    private AccountConverter accountConverter;

    @Override
    public MovementDTO convertEntityToDTO(Movement entity) {
        if (entity == null) return null;

        return MovementDTO.builder()
                .id(entity.getId())
                .date(entity.getDate())
                .type(entity.getType())
                .value(entity.getValue())
                .balance(entity.getBalance())
                .account(accountConverter.convertEntityToDTO(entity.getAccount()))
                .build();
    }

    @Override
    public Movement convertDTOToEntity(MovementDTO dto) {
        if (dto == null) return null;

        return Movement.builder()
                .id(dto.getId())
                .date(dto.getDate())
                .type(dto.getType())
                .value(dto.getValue())
                .balance(dto.getBalance())
                .account(accountConverter.convertDTOToEntity(dto.getAccount()))
                .build();
    }
}
