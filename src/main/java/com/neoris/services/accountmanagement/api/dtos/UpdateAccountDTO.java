package com.neoris.services.accountmanagement.api.dtos;

import com.neoris.services.accountmanagement.api.annotations.EnumValidation;
import com.neoris.services.accountmanagement.api.enums.AccountTypes;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateAccountDTO {

    @NotEmpty(message = "Id is required.")
    private String id;
    @NotEmpty(message = "Type is required.")
    @EnumValidation(
            enumClass = AccountTypes.class,
            ignoreCase = true,
            message = "Type not valid, must be: CURRENT_ACCOUNT or SAVINGS_ACCOUNT.",
            required = true)
    private String type;
    @Positive(message = "InitialBalance not valid.")
    private Long initialBalance = 0L;
}
