package com.neoris.services.accountmanagement.api.services.impl;

import com.neoris.services.accountmanagement.api.entity.Account;
import com.neoris.services.accountmanagement.api.entity.Client;
import com.neoris.services.accountmanagement.api.entity.Movement;
import com.neoris.services.accountmanagement.api.enums.MovementType;
import com.neoris.services.accountmanagement.api.exceptions.BusinessServiceException;
import com.neoris.services.accountmanagement.api.exceptions.NoDataFoundException;
import com.neoris.services.accountmanagement.api.repositories.AccountRepository;
import com.neoris.services.accountmanagement.api.repositories.MovementRepository;
import com.neoris.services.accountmanagement.api.services.MovementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class MovementsServiceImpl implements MovementService {

    @Autowired
    private MovementRepository movementRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Movement save(Movement movement) {
        try {
            // Find the Account
            Account account = accountRepository.findById(movement.getAccount().getId())
                    .orElseThrow(() -> new NoDataFoundException("The Account with ID: " + movement.getAccount().getId() +
                            " Not exists"));

            // Validate the Movement type and calculate the balance
            if (movement.getType().equals(MovementType.CREDIT.toString())) {
                // Set the positive value
                movement.setValue(Math.abs(movement.getValue()));
                // Get the last Movement from Account
                List<Object[]> lastMovement = movementRepository.getLastAccountMovement(account.getId());
                log.info("MovementId: " + lastMovement.get(0)[0] + " Date: " + lastMovement.get(0)[1] + " Balance: " + lastMovement.get(0)[2]);
                if (!lastMovement.isEmpty()) {
                    Long balance = Long.parseLong(lastMovement.get(0)[2].toString());
                    // Calculate the balance
                    movement.setBalance(balance + movement.getValue());
                } else {
                    // The Account don't have movements
                    movement.setBalance(account.getInitialBalance() + movement.getValue());
                }
            } else if (movement.getType().equals(MovementType.DEBIT.toString())) {
                // Set the negative value
                movement.setValue(-Math.abs(movement.getValue()));
                // Get the last Movement from Account
                List<Object[]> lastMovement = movementRepository.getLastAccountMovement(account.getId());
                // Movement Value (Positive)
                Long value = Math.abs(movement.getValue());
                if (!lastMovement.isEmpty()) {
                    Long balance = Long.parseLong(lastMovement.get(0)[2].toString());
                    // Validate it have sufficient founds
                    if (!(value <= balance)) {
                        throw new BusinessServiceException("The Account does not have sufficient funds");
                    }
                    // Calculate the balance
                    movement.setBalance(balance - value);
                } else {
                    // The Account don't have movements, take the initialBalance

                    // Validate it have sufficient founds
                    if (!(value <= account.getInitialBalance())) {
                        throw new BusinessServiceException("The Account does not have sufficient funds");
                    }
                    // Calculate the balance
                    movement.setBalance(account.getInitialBalance() - (Math.abs(movement.getValue())));
                }
            }
            // Set the Account
            movement.setAccount(account);
            // Set the Movement Date
            LocalDateTime serverDateTime = LocalDateTime.of(LocalDate.now(), LocalTime.now());
            movement.setDate(serverDateTime);

            // Return the movement
            return movementRepository.save(movement);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec017]: ClientService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (BusinessServiceException e) {
            log.error("[ERROR:ec0018]: ClientService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec0019]: ClientService:save: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }

    @Override
    public void delete(String movementId) {
        try {
            // Find the Movement
            Movement movement = movementRepository.findById(movementId)
                    .orElseThrow(() -> new NoDataFoundException("The Movement with ID: " + movementId + " Not Exists"));
            // Delete Movement
            movementRepository.delete(movement);
        } catch (NoDataFoundException e) {
            log.error("[ERROR:ec020]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        } catch (Exception e) {
            log.error("[ERROR:ec021]: ClientService:delete: ErrorClass: " +
                    e.getClass().getSimpleName() +
                    " ErrorMessage: " + e.getMessage() +
                    " ErrorCause: " + e.getCause());
            throw e;
        }
    }
}