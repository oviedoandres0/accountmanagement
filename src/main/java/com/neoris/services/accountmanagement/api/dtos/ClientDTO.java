package com.neoris.services.accountmanagement.api.dtos;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ClientDTO extends PersonDTO {

    @NotEmpty(message = "Password is required.")
    @Length(message = "password must be min 8 characters.", min = 8)
    private String password;
    private boolean state;
}
