package com.neoris.services.accountmanagement.api.controllers;

import com.neoris.services.accountmanagement.api.converters.AccountConverter;
import com.neoris.services.accountmanagement.api.converters.UpdateAccountConverter;
import com.neoris.services.accountmanagement.api.dtos.AccountDTO;
import com.neoris.services.accountmanagement.api.dtos.UpdateAccountDTO;
import com.neoris.services.accountmanagement.api.entity.Account;
import com.neoris.services.accountmanagement.api.services.AccountService;
import com.neoris.services.accountmanagement.api.utils.WrapperResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/accounts")
@Validated
@Slf4j
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountConverter accountConverter;

    @Autowired
    private UpdateAccountConverter updateAccountConverter;

    @PostMapping
    public ResponseEntity<WrapperResponse<AccountDTO>> save (@RequestBody @Valid AccountDTO accountDTORequest) {
        Account newAccount = accountService.save(accountConverter.convertDTOToEntity(accountDTORequest));
        AccountDTO newAccountDTO = accountConverter.convertEntityToDTO(newAccount);

        return new WrapperResponse<>(true, "Account created successfully", newAccountDTO)
                    .createResponse(HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<WrapperResponse<UpdateAccountDTO>> update (@RequestBody @Valid UpdateAccountDTO updateAccountDTORequest) {
        Account accountUpdated = accountService.update(updateAccountConverter.convertDTOToEntity(updateAccountDTORequest));
        UpdateAccountDTO updateAccountUpdatedDTO = updateAccountConverter.convertEntityToDTO(accountUpdated);

        return new WrapperResponse<>(true, "Account updated successfully", updateAccountUpdatedDTO)
                .createResponse(HttpStatus.OK);
    }

    @DeleteMapping
    @RequestMapping("/{id}")
    public ResponseEntity<WrapperResponse<String>> delete (@PathVariable("id") String accountId) {
        accountService.delete(accountId);

        return new WrapperResponse<>(true, "Account deleted successfully", "")
                .createResponse(HttpStatus.OK);
    }
}
