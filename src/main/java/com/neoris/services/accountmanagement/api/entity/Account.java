/*
    Diego Oviedo / AccountManagementAPI
 */
package com.neoris.services.accountmanagement.api.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "account")
public class Account implements Serializable {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Column(name = "number", nullable = false, unique = true)
    private Long number;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "initialBalance", nullable = false)
    private Long initialBalance;

    @Column(name = "state", nullable = false)
    private boolean state;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;
}
