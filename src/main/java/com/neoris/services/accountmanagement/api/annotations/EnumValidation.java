package com.neoris.services.accountmanagement.api.annotations;

import com.neoris.services.accountmanagement.api.validators.EnumValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {EnumValidator.class})
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumValidation {

    public abstract String message() default "Invalid Enum Value, This is not permitted";

    public abstract boolean required() default false;

    public abstract Class<?>[] groups() default {};

    public abstract Class<? extends Payload>[] payload() default {};

    public abstract Class<? extends Enum<?>> enumClass();

    public abstract boolean ignoreCase() default false;
}
