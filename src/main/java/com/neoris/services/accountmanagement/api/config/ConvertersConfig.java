package com.neoris.services.accountmanagement.api.config;

import com.neoris.services.accountmanagement.api.converters.AccountConverter;
import com.neoris.services.accountmanagement.api.converters.ClientConverter;
import com.neoris.services.accountmanagement.api.converters.MovementConverter;
import com.neoris.services.accountmanagement.api.converters.UpdateAccountConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConvertersConfig {

    @Bean
    public ClientConverter getClientConverter () {
        return new ClientConverter();
    }

    @Bean
    public AccountConverter getAccountConverter() {
        return new AccountConverter();
    }

    @Bean
    public UpdateAccountConverter getUpdateAccountConverter() {
        return new UpdateAccountConverter();
    }

    @Bean
    public MovementConverter getMovementConverter() { return new MovementConverter(); }
}
