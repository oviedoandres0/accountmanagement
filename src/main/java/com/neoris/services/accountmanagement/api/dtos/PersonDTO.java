package com.neoris.services.accountmanagement.api.dtos;

import com.neoris.services.accountmanagement.api.annotations.EnumValidation;
import com.neoris.services.accountmanagement.api.enums.PersonGender;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public abstract class PersonDTO {

    private String id;
    @NotEmpty(message = "The name is required.")
    private String name;
    @EnumValidation(enumClass = PersonGender.class, ignoreCase = true, message = "Gender not valid, must be: MALE or FEMALE.", required = true)
    @NotEmpty(message = "Gender is required.")
    private String gender;
    @Positive(message = "Age not valid.")
    private int age = 0;
    @Positive(message = "Identification not valid")
    private long identification = 0;
    @NotEmpty(message = "Address is required.")
    private String address;
    @Positive(message = "Phone not valid")
    private long phone = 0;
}
