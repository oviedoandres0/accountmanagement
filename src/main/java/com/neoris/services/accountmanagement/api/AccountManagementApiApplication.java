/*
    Diego Oviedo / AccountManagementAPI
 */
package com.neoris.services.accountmanagement.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class AccountManagementApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountManagementApiApplication.class, args);
	}

}
