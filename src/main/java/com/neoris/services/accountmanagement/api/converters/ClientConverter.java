package com.neoris.services.accountmanagement.api.converters;

import com.neoris.services.accountmanagement.api.dtos.ClientDTO;
import com.neoris.services.accountmanagement.api.entity.Client;

public class ClientConverter extends AbstractConverter<Client, ClientDTO> {

    @Override
    public ClientDTO convertEntityToDTO(Client entity) {
        if (entity == null) return null;

        return ClientDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .gender(entity.getGender())
                .age(entity.getAge())
                .identification(entity.getIdentification())
                .address(entity.getAddress())
                .phone(entity.getPhone())
                .password(entity.getPassword())
                .state(entity.isState())
                .build();
    }

    @Override
    public Client convertDTOToEntity(ClientDTO dto) {
        if (dto == null) return null;

        return Client.builder()
                .id(dto.getId())
                .name(dto.getName())
                .gender(dto.getGender())
                .age(dto.getAge())
                .identification(dto.getIdentification())
                .address(dto.getAddress())
                .phone(dto.getPhone())
                .password(dto.getPassword())
                .state(dto.isState())
                .build();
    }
}
