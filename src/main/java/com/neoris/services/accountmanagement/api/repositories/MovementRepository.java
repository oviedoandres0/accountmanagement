package com.neoris.services.accountmanagement.api.repositories;

import com.neoris.services.accountmanagement.api.entity.Movement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovementRepository extends JpaRepository<Movement, String> {

    @Query(value = "SELECT m.id, m.date, m.balance FROM movement m INNER JOIN account a ON m.account_id = a.id WHERE a.id = ?1 ORDER BY M.date DESC LIMIT 1", nativeQuery = true)
    List<Object[]> getLastAccountMovement(String accountId);
}
