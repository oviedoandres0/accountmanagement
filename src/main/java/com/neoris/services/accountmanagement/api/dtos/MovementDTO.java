package com.neoris.services.accountmanagement.api.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.neoris.services.accountmanagement.api.annotations.EnumValidation;
import com.neoris.services.accountmanagement.api.enums.MovementType;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovementDTO {

    private String id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;
    @EnumValidation(
            enumClass = MovementType.class,
            ignoreCase = true,
            message = "Type not valid, must be: CREDIT or DEBIT.",
            required = true)
    @NotEmpty(message = "Type is required.")
    private String type;
    @Positive(message = "Value not valid.")
    private Long value = 0L;
    private Long balance = 0L;
    @NotNull(message = "Account property is required.")
    private AccountDTO account;
}
