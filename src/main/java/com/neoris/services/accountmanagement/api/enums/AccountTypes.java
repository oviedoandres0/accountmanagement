/*
    Diego Oviedo / AccountManagementAPI
 */
package com.neoris.services.accountmanagement.api.enums;

public enum AccountTypes {
    CURRENT_ACCOUNT,
    SAVINGS_ACCOUNT
}
