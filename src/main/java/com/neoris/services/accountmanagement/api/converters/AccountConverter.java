package com.neoris.services.accountmanagement.api.converters;

import com.neoris.services.accountmanagement.api.dtos.AccountDTO;
import com.neoris.services.accountmanagement.api.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;

public class AccountConverter extends AbstractConverter<Account, AccountDTO>{

    @Autowired
    private ClientConverter clientConverter;

    @Override
    public AccountDTO convertEntityToDTO(Account entity) {
        if (entity == null) return null;

        return AccountDTO.builder()
                .id(entity.getId())
                .number(entity.getNumber())
                .type(entity.getType())
                .initialBalance(entity.getInitialBalance())
                .state(entity.isState())
                .client(clientConverter.convertEntityToDTO(entity.getClient()))
                .build();
    }

    @Override
    public Account convertDTOToEntity(AccountDTO dto) {
        if (dto == null) return null;

        return Account.builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .type(dto.getType())
                .initialBalance(dto.getInitialBalance())
                .state(dto.isState())
                .client(clientConverter.convertDTOToEntity(dto.getClient()))
                .build();
    }
}
