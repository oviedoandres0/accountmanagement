/*
    Diego Oviedo / AccountManagementAPI
 */
package com.neoris.services.accountmanagement.api.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
@Entity
@Table(name = "client")
@PrimaryKeyJoinColumn(name = "personId")
public class Client extends Person implements Serializable {

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "state", nullable = false)
    private boolean state;
}
