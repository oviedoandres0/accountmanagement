package com.neoris.services.accountmanagement.api.config;

import com.neoris.services.accountmanagement.api.exceptions.BusinessServiceException;
import com.neoris.services.accountmanagement.api.exceptions.GeneralServiceException;
import com.neoris.services.accountmanagement.api.exceptions.NoDataFoundException;
import com.neoris.services.accountmanagement.api.exceptions.ValidateServiceException;
import com.neoris.services.accountmanagement.api.utils.WrapperResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ErrorHandlerConfig extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        log.error(ex.getMessage(), ex);
        Map<String, Object> body = new HashMap<>();
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        // Errors Map
        body.put("errors", errors);
        // Make a Response with Wrapper Response
        WrapperResponse<?> response  = new WrapperResponse<>(false, "Bad Request, check your request body", body);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> all(Exception e, WebRequest request) {
        log.error(e.getMessage(), e);
        WrapperResponse<?> response  = new WrapperResponse<>(false, "Internal Server Error", null);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ValidateServiceException.class)
    public ResponseEntity<?> validateServiceException(Exception e, WebRequest request) {
        log.error(e.getMessage(), e);
        WrapperResponse<?> response  = new WrapperResponse<>(false, e.getMessage(), null);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<?> notFoundException(Exception e, WebRequest request) {
        log.error(e.getMessage(), e);
        WrapperResponse<?> response  = new WrapperResponse<>(false, e.getMessage(), null);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GeneralServiceException.class)
    public ResponseEntity<?> generalException(Exception e, WebRequest request) {
        log.error(e.getMessage(), e);
        WrapperResponse<?> response  = new WrapperResponse<>(false, e.getMessage(), null);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BusinessServiceException.class)
    public ResponseEntity<?> businessServiceException(Exception e, WebRequest request) {
        log.error(e.getMessage(), e);
        WrapperResponse<?> response  = new WrapperResponse<>(false, e.getMessage(), null);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
