/*
    Diego Oviedo / AccountManagementAPI
 */
package com.neoris.services.accountmanagement.api.enums;

public enum PersonGender {
    MALE, FEMALE
}
