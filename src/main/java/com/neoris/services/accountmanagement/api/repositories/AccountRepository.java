package com.neoris.services.accountmanagement.api.repositories;

import com.neoris.services.accountmanagement.api.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

    Optional<Account> findByNumber(Long number);
}
