package com.neoris.services.accountmanagement.api.controllers;

import com.neoris.services.accountmanagement.api.converters.ClientConverter;
import com.neoris.services.accountmanagement.api.dtos.AccountReportDTO;
import com.neoris.services.accountmanagement.api.dtos.ClientDTO;
import com.neoris.services.accountmanagement.api.entity.Client;
import com.neoris.services.accountmanagement.api.services.ClientService;
import com.neoris.services.accountmanagement.api.utils.WrapperResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/clients")
@Validated
@Slf4j
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @PostMapping
    public ResponseEntity<WrapperResponse<ClientDTO>> save (@RequestBody @Valid ClientDTO clientDTORequest) {
        Client newClient = clientService.save(clientConverter.convertDTOToEntity(clientDTORequest));
        ClientDTO newClientDTO = clientConverter.convertEntityToDTO(newClient);

        return new WrapperResponse<>(true, "Client created successfully", newClientDTO)
                .createResponse(HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<WrapperResponse<ClientDTO>> update (@RequestBody @Valid ClientDTO clientDTORequest) {
        Client clientUpdated = clientService.update(clientConverter.convertDTOToEntity(clientDTORequest));
        ClientDTO clientUpdatedDTO = clientConverter.convertEntityToDTO(clientUpdated);

        return new WrapperResponse<>(true, "Client updated successfully", clientUpdatedDTO)
                .createResponse(HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/report")
    public ResponseEntity<WrapperResponse<List<AccountReportDTO>>> getReport (
            @RequestParam(name = "clientId") String clientId,
            @RequestParam(name = "startDate")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime startDate,
            @RequestParam(name = "endDate")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime endDate
    ) {
        List<AccountReportDTO> reportList = clientService.getReport(clientId, startDate, endDate);

        return new WrapperResponse<>(true, "Report generated successfully", reportList)
                .createResponse(HttpStatus.OK);
    }

    @DeleteMapping
    @RequestMapping("/{id}")
    public ResponseEntity<WrapperResponse<String>> delete (@PathVariable("id") String clientId) {
        clientService.delete(clientId);
        log.info("Client Deleted Successfully with ID: " + clientId);

        return new WrapperResponse<>(true, "Client deleted successfully", "")
                .createResponse(HttpStatus.OK);
    }
}
