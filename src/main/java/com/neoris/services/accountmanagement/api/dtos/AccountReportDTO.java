package com.neoris.services.accountmanagement.api.dtos;

import lombok.*;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountReportDTO {

    private Timestamp movementDate;
    private String clientName;
    private BigInteger accountNumber;
    private String accountType;
    private BigInteger accountInitialBalance;
    private boolean accountState;
    private BigInteger movementValue;
    private BigInteger movementBalance;
}
