package com.neoris.services.accountmanagement.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neoris.services.accountmanagement.api.converters.AccountConverter;
import com.neoris.services.accountmanagement.api.converters.UpdateAccountConverter;
import com.neoris.services.accountmanagement.api.dtos.AccountDTO;
import com.neoris.services.accountmanagement.api.dtos.ClientDTO;
import com.neoris.services.accountmanagement.api.dtos.UpdateAccountDTO;
import com.neoris.services.accountmanagement.api.entity.Account;
import com.neoris.services.accountmanagement.api.entity.Client;
import com.neoris.services.accountmanagement.api.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AccountController.class)
@AutoConfigureMockMvc(addFilters = false)
@Slf4j
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    private AccountConverter accountConverter;

    @MockBean
    private UpdateAccountConverter updateAccountConverter;

    @Autowired
    private ObjectMapper objectMapper;

    // Class Mocks

    // Request Body
    AccountDTO accountDTORequest = AccountDTO.builder()
            .number(351324125L)
            .type("CURRENT_ACCOUNT")
            .initialBalance(1000000L)
            .client(ClientDTO.builder().id("2a24fd12-5a86-4bcd-8257-397421488eab").build())
            .build();

    // Request Body DTO > Entity
    Account accountEntityRequest = Account.builder()
            .number(351324125L)
            .type("CURRENT_ACCOUNT")
            .initialBalance(1000000L)
            .client(Client.builder().id("2a24fd12-5a86-4bcd-8257-397421488eab").build())
            .build();

    // Response Entity
    Account accountEntityResponse = Account.builder()
            .id("379715e4-82d5-4a08-b3dc-5579574e3658")
            .type("CURRENT_ACCOUNT")
            .initialBalance(1000000L)
            .state(true)
            .client(Client.builder().id("2a24fd12-5a86-4bcd-8257-397421488eab").build())
            .build();

    // Response DTO
    AccountDTO accountDTOResponse = AccountDTO.builder()
            .id("379715e4-82d5-4a08-b3dc-5579574e3658")
            .type("CURRENT_ACCOUNT")
            .initialBalance(1000000L)
            .state(true)
            .client(ClientDTO.builder().id("2a24fd12-5a86-4bcd-8257-397421488eab").build())
            .build();

    // Request Body UpdateAccount
    UpdateAccountDTO updateAccountDTORequest = UpdateAccountDTO.builder()
            .id("379715e4-82d5-4a08-b3dc-5579574e3658")
            .type("SAVINGS_ACCOUNT")
            .initialBalance(200000L)
            .build();

    Account accountEntity = Account.builder()
            .id("379715e4-82d5-4a08-b3dc-5579574e3658")
            .type("SAVINGS_ACCOUNT")
            .initialBalance(200000L)
            .build();

    UpdateAccountDTO updateAccountDTOResponse = UpdateAccountDTO.builder()
            .id("379715e4-82d5-4a08-b3dc-5579574e3658")
            .type("SAVINGS_ACCOUNT")
            .initialBalance(200000L)
            .build();

    Account accountUpdatedEntityResponse = Account.builder()
            .id("379715e4-82d5-4a08-b3dc-5579574e3658")
            .type("SAVINGS_ACCOUNT")
            .initialBalance(200000L)
            .build();

    @Test
    public void testCreateAccount() throws Exception {

        Mockito.when(accountConverter.convertDTOToEntity(accountDTORequest)).thenReturn(accountEntityRequest);
        Mockito.when(accountService.save(accountEntityRequest)).thenReturn(accountEntityResponse);
        Mockito.when(accountConverter.convertEntityToDTO(accountEntityResponse)).thenReturn(accountDTOResponse);

        mockMvc.perform(post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(accountDTORequest)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success", Matchers.is(true)))
                .andExpect(jsonPath("$.message", Matchers.is("Account created successfully")))
                .andDo(print());
    }

    @Test
    public void testUpdateAccount() throws Exception {

        Mockito.when(updateAccountConverter.convertDTOToEntity(updateAccountDTORequest)).thenReturn(accountEntity);
        Mockito.when(accountService.update(accountEntity)).thenReturn(accountUpdatedEntityResponse);
        Mockito.when(updateAccountConverter.convertEntityToDTO(accountUpdatedEntityResponse)).thenReturn(updateAccountDTOResponse);

        mockMvc.perform(put("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateAccountDTORequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success", Matchers.is(true)))
                .andExpect(jsonPath("$.message", Matchers.is("Account updated successfully")))
                .andDo(print());
    }

    @Test
    public void testDeleteAccount() throws Exception {
        String clientId = "379715e4-82d5-4a08-b3dc-5579574e3658";
        doNothing().when(accountService).delete(clientId);
        mockMvc.perform(delete("/accounts/{id}", clientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", Matchers.is(true)))
                .andExpect(jsonPath("$.message", Matchers.is("Account deleted successfully")))
                .andDo(print());
    }

}
