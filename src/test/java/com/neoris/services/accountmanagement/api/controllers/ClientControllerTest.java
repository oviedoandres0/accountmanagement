package com.neoris.services.accountmanagement.api.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.neoris.services.accountmanagement.api.converters.ClientConverter;
import com.neoris.services.accountmanagement.api.dtos.ClientDTO;
import com.neoris.services.accountmanagement.api.entity.Client;
import com.neoris.services.accountmanagement.api.services.ClientService;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ClientController.class)
@AutoConfigureMockMvc(addFilters = false)
@Slf4j
public class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    @MockBean
    private ClientConverter clientConverter;

    @Autowired
    private ObjectMapper objectMapper;

    // Class Mocks

    // Request Body
    ClientDTO clientDTORequest = ClientDTO.builder()
            .name("Diego Oviedo")
            .gender("MALE")
            .age(21)
            .identification(1234567899)
            .address("Calle 134")
            .phone(3124556744L)
            .password("abcd.124565678")
            .build();

    // Request Body DTO > Entity
    Client clientEntityRequest = Client.builder()
            .name("Diego Oviedo")
            .gender("MALE")
            .age(21)
            .identification(1234567899)
            .address("Calle 134")
            .phone(3124556744L)
            .password("abcd.124565678")
            .build();

    // Response entity
    Client clientEntityResponse = Client.builder()
            .id("2a24fd12-5a86-4bcd-8257-397421488eab")
            .name("Diego Oviedo")
            .gender("MALE")
            .age(21)
            .identification(1234567899)
            .address("Calle 134")
            .phone(3124556744L)
            .password("$2a$10$F7OT3wrdxFO7dkxcdVlwZe9Ytj18goUkR/NDBNqOULbrClRC4IjTG")
            .state(true)
            .build();

    // Response DTO
    ClientDTO clientDTOResponse = ClientDTO.builder()
            .id("2a24fd12-5a86-4bcd-8257-397421488eab")
            .name("Diego Oviedo")
            .gender("MALE")
            .age(21)
            .identification(1234567899)
            .address("Calle 134")
            .phone(3124556744L)
            .password("$2a$10$F7OT3wrdxFO7dkxcdVlwZe9Ytj18goUkR/NDBNqOULbrClRC4IjTG")
            .state(true)
            .build();

    @Test
    public void testCreateClient() throws Exception {

        Mockito.when(clientConverter.convertDTOToEntity(clientDTORequest)).thenReturn(clientEntityRequest);
        Mockito.when(clientService.save(clientEntityRequest)).thenReturn(clientEntityResponse);
        Mockito.when(clientConverter.convertEntityToDTO(clientEntityResponse)).thenReturn(clientDTOResponse);

        mockMvc.perform(post("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(clientDTORequest)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success", Matchers.is(true)))
                .andExpect(jsonPath("$.message", Matchers.is("Client created successfully")))
                .andDo(print());
    }

    @Test
    public void testUpdateClient() throws Exception {

        Mockito.when(clientConverter.convertDTOToEntity(clientDTOResponse)).thenReturn(clientEntityResponse);
        Mockito.when(clientService.update(clientEntityResponse)).thenReturn(clientEntityResponse);
        Mockito.when(clientConverter.convertEntityToDTO(clientEntityResponse)).thenReturn(clientDTOResponse);

        mockMvc.perform(put("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(clientDTORequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success", Matchers.is(true)))
                .andExpect(jsonPath("$.message", Matchers.is("Client updated successfully")))
                .andDo(print());
    }

    @Test
    public void testDeleteClient() throws Exception {
        String clientId = "2a24fd12-5a86-4bcd-8257-397421488eab";
        doNothing().when(clientService).delete(clientId);
        mockMvc.perform(delete("/clients/{id}", clientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", Matchers.is(true)))
                .andExpect(jsonPath("$.message", Matchers.is("Client deleted successfully")))
                .andDo(print());
    }

}
