FROM adoptopenjdk/openjdk11
ADD target/account-management-api-1.0.0.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]